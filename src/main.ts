import http from "http";
import https from "https";

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello, World!\n');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

console.log("I am here");


const request: (url: string, method?: string, postData?: any) => Promise<string>  = async (url, method = 'GET', postData) => {
  //await Promise.resolve();
  const lib = url.startsWith('https://') ? https : http;
  const [h, path] = url.split('://')[1].split('/');
  const [host, port] = h.split(':');

  const params = {
    method,
    host,
    port: port || url.startsWith('https://') ? 443 : 80,
    path: path || '/',
  };

  return new Promise(async (resolve, reject) => {
    //await Promise.resolve();
    console.log("Request started");
    const req = lib.request(params, res => {
      if (res.statusCode! < 200 || res.statusCode! >= 300) {
        return reject(new Error(`Status Code: ${res.statusCode}`));
      }

      const data: any[] = [];

      res.on('data', chunk => {
        data.push(chunk);
      });

      res.on('end', () => resolve(Buffer.concat(data).toString()));
    });

    req.on('error', reject);

    if (postData) {
      req.write(postData);
    }

    // IMPORTANT
    req.end();
  });
};

(async () => {
  try {
    const url = "https://www.google.com";
    const req = request(url);
    console.log("After creating request")
    const data = await req;
    console.log("Request Finished");
    //console.log(data);
  } catch (error) {
    console.error(error);
  }
})();