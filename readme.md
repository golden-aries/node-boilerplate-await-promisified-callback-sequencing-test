# This repo is a demonstration/test of promisified native node http/https request and async/await features of javascript.
The test subject is line 34 or 21 in main.ts. //await Promise.resolve(); 
## When the line 34 or 21 is commented - 21: //await Promise.resolve(); 
the sequence of the output is:
```
I am here
Request started
After creating request
Server running at http://127.0.0.1:3000/
Request Finished
```

## When the line 21 is uncommented - 21: await Promise.resolve();
the sequence is:
```
I am here
After creating request
Server running at http://127.0.0.1:3000/
Request started
Request Finished
```

Notice that "After creating request" was reached before request started.

## References
I borrowed the code for promisfying native node request from Gevorg A. Galstyan
[How to promisify node.js http/https requests Gevorg A. Galstyan Apr 4, 2019 · 1 min read](
    https://medium.com/@gevorggalstyan/how-to-promisify-node-js-http-https-requests-76a5a58ed90c
)